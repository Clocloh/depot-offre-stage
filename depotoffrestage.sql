-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 30 Mars 2018 à 10:11
-- Version du serveur :  10.1.16-MariaDB
-- Version de PHP :  7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `depotoffrestage`
--

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

CREATE TABLE `entreprise` (
  `id` int(255) NOT NULL,
  `nom_contact` varchar(50) NOT NULL,
  `raison_sociale` varchar(50) NOT NULL,
  `telephone` int(10) NOT NULL,
  `adresse` varchar(50) NOT NULL,
  `code_postal` int(5) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `fax` int(10) NOT NULL,
  `site` varchar(50) NOT NULL,
  `intitule` int(50) NOT NULL,
  `stage_entreprise` tinyint(1) NOT NULL,
  `stage_teletravail` tinyint(1) NOT NULL,
  `duree_stage` int(11) NOT NULL,
  `renumeration` int(11) NOT NULL,
  `bdd` varchar(200) NOT NULL,
  `methode` varchar(200) NOT NULL,
  `langage` varchar(200) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `type` varchar(100) NOT NULL,
  `taille` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `entreprise_modele`
--

CREATE TABLE `entreprise_modele` (
  `id` int(255) NOT NULL,
  `nom_contact` varchar(50) NOT NULL,
  `raison_sociale` varchar(50) NOT NULL,
  `adresse` varchar(100) NOT NULL,
  `ville` varchar(100) NOT NULL,
  `cp` int(100) NOT NULL,
  `email_client` varchar(100) NOT NULL,
  `fax` int(100) NOT NULL,
  `site` varchar(100) NOT NULL,
  `intitule` varchar(100) NOT NULL,
  `stage_entreprise` tinyint(100) NOT NULL,
  `stage_teletravail` tinyint(100) NOT NULL,
  `duree_stage` int(100) NOT NULL,
  `remuneration` int(100) NOT NULL,
  `bdd` varchar(100) NOT NULL,
  `methode` varchar(100) NOT NULL,
  `langage` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `entreprise_modele`
--

INSERT INTO `entreprise_modele` (`id`, `nom_contact`, `raison_sociale`, `adresse`, `ville`, `cp`, `email_client`, `fax`, `site`, `intitule`, `stage_entreprise`, `stage_teletravail`, `duree_stage`, `remuneration`, `bdd`, `methode`, `langage`) VALUES
(1, 'robert', 'SUADEO', '', '', 0, '', 0, '', '0', 0, 0, 0, 0, '', '', ''),
(2, 'totoro', 'MUNIVIE', '', '', 0, '', 0, '', '0', 0, 0, 0, 0, '', '', ''),
(3, 'virginie', 'tre', 'eaubonne', '', 0, '', 0, '', '0', 0, 0, 0, 0, '', '', ''),
(4, 'ss', 'ee', 'qq', 'rree', 45, '', 0, '', '0', 0, 0, 0, 0, '', '', ''),
(5, 'df', 'rr', 'zz', 'zz', 45, 'fdd@df.com', 0, '', '0', 0, 0, 0, 0, '', '', ''),
(6, 'fs', 'dq', 'fr', 'rr', 45, 'dd@gm.com', 0, 'gg', '0', 0, 0, 0, 0, '', '', ''),
(7, 'sdssdss', 'sfsfs', 'frszs', 'frs', 55, 'fdd@df.com', 0, 'dss', '0', 0, 0, 0, 0, '', '', ''),
(8, 'sdssdss', 'sfsfs', 'frszs', 'frs', 55, 'fdd@df.com', 0, 'dss', '4', 0, 0, 0, 0, '', '', ''),
(9, 'gfdg', 'dd', 's', '', 45, 'dds@gm.co', 0, 'qdq', '8', 0, 0, 0, 0, '', '', ''),
(10, 'gfdg', 'dd', 's', '', 45, 'dds@gm.co', 0, 'qdqcoche', '8', 0, 0, 0, 0, '', '', ''),
(11, 'gfdg', 'dd', 's', '', 45, 'dds@gm.co', 0, 'tjrs', '8', 0, 0, 0, 0, '', '', ''),
(12, 'gfdg', 'dd', 's', '', 45, 'dds@gm.co', 0, 'decoche', '8', 0, 1, 0, 0, '', '', ''),
(13, '', 'gffgf', '', '', 0, 'gfgf', 0, '', '55', 0, 1, 5, 5555555, '', '', ''),
(14, '', 'ghgfh', '', '', 0, 'ghfh', 0, '', '55', 0, 1, 5, 0, 'OVH', 'agil', ''),
(15, '', 'nouveau', '', '', 0, 'dfdf', 0, '', '85', 0, 1, 5, 0, '', '', 'ghhhh'),
(16, '', 'SUADEO', 'qq', 'yy', 87056, 'fdd@df.com', 0, '', '0', 0, 1, 5, 0, 'saab', 'volvo', 'volvo'),
(17, 'F', '5', 'r', 'yr', 78, 'fdd@df.com', 2147483647, 'dd', '55', 0, 1, 5, 5555555, 'MySql', 'Donnéees', 'java'),
(18, 'df', 'HEY', 'zz', 'zz', 45, 'fdd@df.com', 5565656, 'ddfdfdf.com', '59632154', 0, 1, 5, 5585, 'MySql', 'Donnéees', 'java'),
(19, 'vi', 'HERYE', 'bheyhyshys', 'paris', 87056, 'bonours@gmgf.com', 5565656, 'dsfsdfds', '0', 0, 1, 6, 5555555, 'ovh', 'Agile', 'php'),
(20, 'cvxcvxv', 'cvcv', 'cvxcvxxcv', 'cvxcvxcv', 8556, 'fdd@df.com', 0, 'xdfsdf', '0', 0, 1, 5, 5555555, 'Array', 'Donnéees', 'javascript'),
(21, 'fdf', 'gf', 'gfg', 'paris', 48, 'fdd@df.com', 2147483647, 'gg', '0', 0, 1, 5, 5555555, 'MySql', 'Agile', 'html'),
(22, 'df', 'dfsdf', 'zz', 'zz', 45, 'fdd@df.com', 0, '', '0', 0, 1, 5, 0, 'MySql', 'Agile', 'html'),
(23, 'vi', 'ghghgh', 'bheyhyshys', 'paris', 87056, 'ghgf', 0, '', '55', 0, 1, 5, 0, 'MySql', 'Agile', 'html'),
(24, 'hgf', 'hgh', 'gfgh', '', 0, 'gh', 0, '', '0', 0, 1, 54, 0, 'MySql', 'Donnéees', 'php'),
(25, 'gfqzdf', 'f', 'dfdg', 'gf', 5466, 'gfd@flflf.vmvm', 54544, 'dfsdfsd', 'sdfsdf', 0, 1, 8, 545454, 'ovh', 'Agile', 'html'),
(26, 'gt', 'dd', 'rr', 'yy', 45, 'dfgdddg@mgmg.glgl', 54454, 'dfsdfsd', 'fsfsd', 0, 1, 1, 5555, 'ovh', 'Agile', 'html');

-- --------------------------------------------------------

--
-- Structure de la table `fichier`
--

CREATE TABLE `fichier` (
  `nom` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `email_client` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `type` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `taille` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Contenu de la table `fichier`
--

INSERT INTO `fichier` (`nom`, `email_client`, `type`, `taille`) VALUES
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', 'hey', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', 'hey', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', 'hey', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', 'hey', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', 'hey', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', 'gfhhg@ggg.com', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', 'gfhhg@ggg.com', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', 'gfhhg@ggg.com', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', '', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', '', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', '', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', '', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', '', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', '', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', 'sb@g.com', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', 'dd', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', 'dd', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', 'fdd@df.com', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', 'bonours@gmgf.com', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', 'fdd@df.com', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', 'fdd@df.com', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadsMaquette.docx', 'ghgf', ' docx', 38941),
('C:xampphtdocsdepotoffredepotoffrestageuploadssource.docx', 'gh', ' docx', 13009),
('C:xampphtdocsdepotuploadsPrÃ©sentation du site.docx', 'gfd@flflf.vmvm', ' docx', 13913),
('C:xampphtdocsdepotoffredepotoffrestageuploadsPrÃ©sentation du site.docx', 'dfgdddg@mgmg.glgl', ' docx', 13913);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `entreprise`
--
ALTER TABLE `entreprise`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `entreprise_modele`
--
ALTER TABLE `entreprise_modele`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `entreprise`
--
ALTER TABLE `entreprise`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `entreprise_modele`
--
ALTER TABLE `entreprise_modele`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
